
export interface Coordinate {
  xCoordinate: number;
  yCoordinate: number;
}

export type Grid = Array<Array<Point>>

export interface Point {
  value: number;
  isHidden: boolean;
}

export interface RevealPointResolution {
  newGrid: Grid;
  mineFound: boolean;
  hasWon: boolean;
}

const generateMineCoordinate = (x: number, y: number): Coordinate => {
  const xCoordinate = Math.floor(Math.random() * x);
  const yCoordinate = Math.floor(Math.random() * y);
  return { xCoordinate, yCoordinate };
};

const generateInitialGrid = (x: number, y: number): Grid => {
  const grid = [];
  for (let i = 0; i < x; i += 1) {
    grid[i] = new Array(y).fill({
      value: 0,
      isHidden: true
    });
  }
  return grid;
};

const placeMines = (grid: Grid, numberMines: number): void => {
  let minesPlaced = 0;
  while (minesPlaced < numberMines) {
    const coordinate = generateMineCoordinate(grid.length, grid[0].length);
    const { xCoordinate, yCoordinate } = coordinate;
    const gridCoord = grid[xCoordinate][yCoordinate];
    if (gridCoord.value !== 9) {
      // use Array.prototype.splice() to avoid direct object property mutation issues
      grid[xCoordinate].splice(yCoordinate, 1, {
        ...grid[xCoordinate][yCoordinate],
        value: 9
      });
      minesPlaced += 1;
    }
  }
};

const calculateMines = (grid: Grid): void => {
  for (let x = 0; x < grid.length; x += 1) {
    for (let y = 0; y < grid[0].length; y += 1) {
      // continue if mine already present
      if (grid[x][y].value === 9) {
        continue;
      }
      let mineCount = 0;
      // calculate mine count by looking at all adjacent coordinates
      for (let i = -1; i <= 1; i += 1) {
        if (x + i < 0 || x + i >= grid.length) {
          continue;
        }
        for (let j = -1; j <= 1; j += 1) {
          if (y + j < 0 || y + j >= grid[0].length) {
            continue;
          }
          if (grid[x + i][y + j].value === 9) {
            mineCount += 1;
          }
        }
      }
      grid[x].splice(y, 1, {
        ...grid[x][y],
        value: mineCount
      });
    }
  }
}

const revealNonZeroPoints = (grid: Grid, x: number, y: number): void => {
  const range = 1;
  for (let i = -range; i <= range; i += 1) {
    if (x + i < 0 || x + i >= grid.length) {
      continue;
    }
    for (let j = -range; j <= range; j += 1) {
      if (y + j < 0 || y + j >= grid[0].length) {
        continue;
      }

      const point = grid[x + i][y + j];
      if (point.isHidden) {
        grid[x + i].splice(y + j, 1, {
          ...point,
          isHidden: false
        });
        if (point.value === 0) {
          revealNonZeroPoints(grid, x + i, y + j);
        }
      }
    }
  }
};

export const createGrid = (x = 5, y = 7, numberMines = 2): Grid => {
  const grid = generateInitialGrid(x, y);
  placeMines(grid, numberMines);
  calculateMines(grid);
  return grid;
};

export const resolveShowPoint = (grid: Grid, x: number, y: number, numberMines: number): RevealPointResolution => {
  let newGrid: Grid = [...grid];
  const selectedPoint = grid[x][y];
  const mineFound = selectedPoint.value === 9;

  // reveal all points if mine found
  // otherwise, if value is 0, reveal until non-zero found
  // else, just reveal the one point
  if (mineFound) {
    newGrid = newGrid.map((x: Point[]): Point[] => {
      return x.map((point: Point): Point => ({
        ...point,
        isHidden: false
      }));
    });
  } else if (selectedPoint.value === 0) {
    revealNonZeroPoints(newGrid, x, y);
  } else {
    newGrid[x].splice(y, 1, {
      ...selectedPoint,
      isHidden: false
    });
  }

  const hasWon: boolean = newGrid.map((x: Point[]): Point[] => {
    return x.filter((point: Point): boolean => point.isHidden);
  }).flat().length === numberMines;

  return {
    newGrid,
    mineFound,
    hasWon
  };
};
