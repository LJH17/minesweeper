import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';

import './styles.css';

import { createGrid, resolveShowPoint, Grid } from './minesweeperGrid';
import { GridCol } from './GridCol';
import { Option } from './Option';

const App: React.FC = () => {
  const name = 'Luke Hooker';
  const [rows, setRows] = useState<number>(7);
  const [columns, setColumns] = useState<number>(5);
  const [mines, setMines] = useState<number>(3);
  const [grid, setGrid] = useState<Grid>(createGrid(rows, columns, mines));
  const [moveCount, setMoveCount] = useState<number>(0);
  const [mineFound, setMineFound] = useState<boolean>(false);
  const [victory, setVictory] = useState<boolean>(false);

  const decrementGrid = (decFunc: () => void): void => {
    setMines(3);
    decFunc();
  }

  const handleShowPoint = (x: number, y: number): void => {
    const { newGrid, mineFound, hasWon } = resolveShowPoint(grid, x, y, mines);
    setGrid(newGrid);
    setMoveCount(moveCount + 1);
    setMineFound(mineFound);
    setVictory(hasWon);
  };

  const resetGame = (): void => {
    setMoveCount(0);
    setMineFound(false);
    setVictory(false);
    setGrid(createGrid(columns, rows, mines));
  };

  useEffect(() => {
    setGrid(createGrid(columns, rows, mines));
  }, [rows, columns, mines]);

  const disableOptions = mineFound || victory;

  return (
    <div className="App">
      <h1>Minesweeper</h1>
      <h3>by { name }</h3>
      <Option
        disabled={disableOptions}
        label="Rows"
        value={rows}
        decrementFunc={() => rows > 3 && decrementGrid(() => setRows(rows - 1))}
        incrementFunc={() => setRows(rows + 1)}
      />
      <Option
        disabled={disableOptions}
        label="Columns"
        value={columns}
        decrementFunc={() => columns > 3 && decrementGrid(() => setColumns(columns - 1))}
        incrementFunc={() => setColumns(columns + 1)}
      />
      <Option
        disabled={disableOptions}
        label="Mines"
        value={mines}
        decrementFunc={() => mines > 1 && setMines(mines - 1)}
        incrementFunc={() => (mines < rows * columns - 1) && setMines(mines + 1)}
      />
      <button type="button" className="button--reset" onClick={resetGame}>Reset</button>
      { mineFound && <div className="pt-4">Game Over</div> }
      { victory && <div className="pt-4">Congratulations!</div> }
      <div className="pt-4">Move Count: {moveCount}</div>
      <div className="container">
        {
          grid.map((col, x) => (
            <GridCol
              col={col}
              key={x}
              onShowPoint={(y) => handleShowPoint(x, y)}
            />
          ))
        }
      </div>
    </div>
  );
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);
