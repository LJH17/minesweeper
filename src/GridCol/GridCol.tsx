import React from 'react';

import { Point } from '../minesweeperGrid';

import styles from './GridCol.module.css';

export interface GridColProps {
  col: Point[];
  onShowPoint: (y: number) => void;
}

export const GridCol: React.FC<GridColProps> = ({ col, onShowPoint }) => {
  const createPointClass = (point: Point) => {
    let classString = `${styles.point}`;
    classString += point.isHidden
      ? ` ${styles['point--hidden']}`
      : point.value === 9
      ? ` ${styles['point--exploded']}`
      : point.value === 0
      ? ` ${styles['point--safe']}`
      : ` ${styles['point--warning']}`;
    return classString;
  };

  return (
    <div className={styles.col}>
      {
        col.map((y, i) => (
          <button
            key={i}
            className={createPointClass(y)}
            onClick={() => onShowPoint(i)}
          >
            { y.isHidden ? 'X' : y.value }
          </button>
        ))
      }
    </div>
  );
}
