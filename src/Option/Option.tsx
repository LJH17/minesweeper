import React from 'react';

import styles from './Option.module.css';

export interface OptionProps {
  disabled: boolean;
  label: string;
  value: number;
  decrementFunc: () => void;
  incrementFunc: () => void;
}

export const Option: React.FC<OptionProps> = ({
  disabled,
  label,
  value,
  decrementFunc,
  incrementFunc
}) => (
  <div className={styles.option}>
    <button className={!disabled ? styles.option__button : ''} disabled={disabled} type="button" onClick={decrementFunc}>
      -
    </button>
    <span className={styles.option__text}>{ label }: { value }</span>
    <button className={!disabled ? styles.option__button : ''} disabled={disabled} type="button" onClick={incrementFunc}>
      +
    </button>
  </div>
);
